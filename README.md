# Лабораторная работа
Рогачёва Дарья P33111

Вариант: `asm | acc | neum | hw | instr | struct | trap | port | prob2`


## Структура проекта
Исполняемые файлы:
- `bin/translator.py` - бинарник, который получает на вход код `.asm`, а выводит бинарник `.bin`
- `bin/run.py` - виртуальная машина, которая будет исполнять код, на вход получает `.bin` и файл с входными данными `.txt`

Прочее:
- `machine/isa.py` - instruction set architecture, описывает опкоды и архитектуру команд
- `machine/simulation.py` - содержит реализацию виртуальной машины
- `machine/translator.py` - реализация транслятора из ассемблера в машинный код


## Описание языка
Упрощённый язык ассемблера в расширенной форме Бэкуса — Наура:
```
<letter> ::= "A" | "B" | "C" | "D" | "E" | "F" | "G"
       | "H" | "I" | "J" | "K" | "L" | "M" | "N"
       | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
       | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
       | "c" | "d" | "e" | "f" | "g" | "h" | "i"
       | "j" | "k" | "l" | "m" | "n" | "o" | "p"
       | "q" | "r" | "s" | "t" | "u" | "v" | "w"
       | "x" | "y" | "z" | "_"
<name> ::= <letter> <name> | <letter>
<label> ::= "$" <name> | "." <name> | <name>

<digit> ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" 
<number> ::= <digit> <number> | <digit>

<empty_expr> ::= "\n"

<op_0_arg> ::= "hlt" | "in" | "out" | "iret"
<op_1_arg> ::= "st" | "ld" | "add" | "mul" | "div" | "mod" | "cmp" | "jmp" | "je" | "word" | "db" | 
               "jnz" | "jb"
<label_def> ::= <name> ":" | "section .text" | "section .data"

<line> ::= <label_def> | <op_0_arg> | <op_1_arg> " " <label> |  <op_1_arg> " " <label> | <empty_expr>

<program> ::= <line> <program> | <line>
```


## ISA/mnemonics

У операндов поддержано три режима адресации:

| мнемоника          | количество тактов                                                                            |
|:-------------------|:---------------------------------------------------------------------------------------------|
| `CONSTANT`         | операнд хранится непосредственно в команде                                                   |
| `DIRECT_ADDRESS`   | операнд - это значение, лежащее по адресу, хранящемся в команде                              |
| `INDIRECT_ADDRESS` | операнд - это значение, лежащее по адресу, хранящемся в ячейке, на которую указывает команда |


Система команд:

| мнемоника | минимальное количество тактов | тип операнда                      | описанаие команды                           	|
|:----------|-------------------------------|:----------------------------------|:--------------------------------------------	|
| `out`     | 2                             | `-`                               | распечатать в output stream значение из acc 	|
| `in`      | 2                             | `-`                               | прочитать в acc значение из input stream    	|
| `hlt`     | -                             | `-`                               | поставить процессор на паузу                	|
| `ld`      | 2                             | `const/direct_addr/inderect_addr` | загрузить значение в acc                    	|
| `st`      | 2                             | `direct_addr/inderect_addr`       | сохранить значение из acc в память          	|
| `add`     | 2                             | `const/direct_addr/inderect_addr` | прибавить к acc аргумент                   	|
| `mul`     | 2                             | `const/direct_addr/inderect_addr` | умножить acc на значение                   	|
| `div`     | 2                             | `const/direct_addr/inderect_addr` | целочисленно поделить acc на значение      	|
| `mod`     | 2                             | `const/direct_addr/inderect_addr` | получить остаток от деления acc на значение 	|
| `cmp`     | 1                             | `const/direct_addr/inderect_addr` | сравнить acc со значением                  	|
| `iret`    | 1                             | `direct_addr/inderect_addr`       | возврат на метку, когда закончилось прерывание   |
| `jmp`     | 1                             | `direct_addr`                     | безусловный переход на аргумент-метку       	|
| `je`      | 1                             | `direct_addr`                     | переход на аргумент-метку, если равно       	|
| `jnz`     | 1                             | `direct_addr`                     | переход на аргумент-метку, если ZF != 0     	|
| `jb`      | 1                             | `direct_addr`                     | переход на аргумент-метку, если NF > 0      	|


Специальные метки:

    start  точка входа
    int_handler    обработчик прерывания (ввод)

Организация памяти

    ____________________ 
    | START_ADDR       |
    | INT_HANDLER_ADDR | 
    | ...              |
    | ...              |
    | STACK BEGINNING  |
    |                  |
    |__________________| 
    
- Машинное слово - 32 бит, знаковое
- Основная память
  - адресуется через ```Address Register (AR)```
  - может быть записана
    - с порта ввода
    - с аккумулятора ```acc``` через операцию WR
  - может быть прочитана в аккумулятор ```acc``` 

## Транслятор
Транслирует код на языке ассемблера в машинные инструкции (`.json`)


Пример исходного кода `hello.asm`:
```asm
section .data
    curr_char: word 1
    hellow: db "hello world"
    null_term: word 0

section .text
    int_handler:
        in
        out
    start:
    print:
        ld $curr_char  ; загружаю символ по адресу curr_char
        cmp #0  ; сравниваю, закончилась ли строка
        je .exit
        out
        ld curr_char
        add #1
        st curr_char
        jmp .print
    exit:
        hlt


```

Результат трансляции `hello.bin`:
```json
{
    "code": [
        1,
        104,
        101,
        108,
        108,
        111,
        32,
        119,
        111,
        114,
        108,
        100,
        0,
        [
            "in",
            null
        ],
        [
            "out",
            null
        ],
        [
            "ld",
            [
                "indirect_address",
                0
            ]
        ],
        [
            "cmp",
            [
                "constant",
                0
            ]
        ],
        [
            "je",
            [
                "constant",
                23
            ]
        ],
        [
            "out",
            null
        ],
        [
            "ld",
            [
                "direct_address",
                0
            ]
        ],
        [
            "add",
            [
                "constant",
                1
            ]
        ],
        [
            "st",
            [
                "constant",
                0
            ]
        ],
        [
            "jmp",
            [
                "constant",
                15
            ]
        ],
        [
            "hlt",
            null
        ]
    ],
    "important_labels": {
        "start": 15,
        "int_handler": 0
    }
}
```



## Модель процессора

Особенности процессора:
- Все операции построены вокруг аккумулятора `acc`, участвует в вводе-выводе
- Ввод-вывод осуществляется посимвольно через порты
- Машинное слово размером в 32 бит
- `instr_ptr` - счетчик инструкций, указывает на адрес исполняемой команды
- Реализована архитектура Фон Неймана:
   Память общая для команд и данных. Каждая ячейка является либо словарем, описывающим инструкцию, либо числом. 
    - `instr_ptr` - счетчик инструкций, указывает на адрес исполняемой команды
    - `stack_pointer` - указывает на вершину стека
    - `addr_reg` - для хранения адреса ячейки
    - `data_reg` - для хранения слова из памяти или для записи в память
 - Реализована система прерываний

## DataPath

![Processor Scheme](doc/vm.jpg)

Сигналы:

- `latch_aсс` -- ащелкнуть выбранное значение в ACC
- `latch_mem` -- защелкнуть результат вычисления АЛУ
- `latch_instr_ptr` -- сигнал для обновления счётчика команд
- `latch_stack_pointer` -- сигнал для обновления указателя стека

Флаги:
- `ZF` -- отражает наличие нулевого значения на выходе АЛУ. Используется для условных переходов.
- `NF` -- отражает знак значения на выходе АЛУ. Используется для условных переходов.

## ControlUnit
Реализован в классе `ControlUnit`.

- Hardwired.
- Моделирование на уровне инструкций.
- Трансляция инструкции в последовательность сигналов: `decode_and_execute_instruction`.


#### Прерывания
- Система прерываний реализована через проверку наличия запроса на прерывание в начале цикла выборки инструкции.
- Прерывания обслуживаются относительно: при поступлении сигнала прерывания во время нахождения в прерывании сигнал будет проигнорирован.
- При прерывании по адресу `SP` сохраняется счетчик команд `PC`, `SP` декрементируется.
-Далее новое значение `PC` берется из памяти данных по адресу обработчика прерываний .
-При обработке инструкции `IRET` значение `SP` инкрементируется и мы получаем адрес возвращения в основную программу

Журнал работы виртуальной машины для программы `hello.out`:
```
machine started
{'code': [0, ['in', None], ['out', None], ['iret', None], ['hlt', None], ['jmp', ['constant', 5]]], 'important_labels': {'start': 5, 'int_handler': 1}}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 1, PC: 5, ADDR: 0, ACC: 0, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 2, PC: 5, ADDR: 0, ACC: 0, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 3, PC: 5, ADDR: 0, ACC: 0, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 4, PC: 6, ADDR: 31, ACC: 0, DR: 5}
{TICK: 5, PC: 1, ADDR: 31, ACC: 0, DR: 5}
{starting executing: ['in', None]}
{TICK: 6, PC: 1, ADDR: 31, ACC: 104, DR: 5}
{TICK: 7, PC: 2, ADDR: 31, ACC: 104, DR: 5}
{starting executing: ['out', None]}
{new output symbol 'h' will be added to ''}
{TICK: 8, PC: 2, ADDR: 31, ACC: 104, DR: 5}
{TICK: 9, PC: 3, ADDR: 31, ACC: 104, DR: 5}
{starting executing: ['iret', None]}
{TICK: 10, PC: 5, ADDR: 31, ACC: 104, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 11, PC: 5, ADDR: 31, ACC: 104, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 12, PC: 6, ADDR: 31, ACC: 104, DR: 5}
{TICK: 13, PC: 1, ADDR: 31, ACC: 104, DR: 5}
{starting executing: ['in', None]}
{TICK: 14, PC: 1, ADDR: 31, ACC: 101, DR: 5}
{TICK: 15, PC: 2, ADDR: 31, ACC: 101, DR: 5}
{starting executing: ['out', None]}
{new output symbol 'e' will be added to 'h'}
{TICK: 16, PC: 2, ADDR: 31, ACC: 101, DR: 5}
{TICK: 17, PC: 3, ADDR: 31, ACC: 101, DR: 5}
{starting executing: ['iret', None]}
{TICK: 18, PC: 5, ADDR: 31, ACC: 101, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 19, PC: 5, ADDR: 31, ACC: 101, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 20, PC: 5, ADDR: 31, ACC: 101, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 21, PC: 5, ADDR: 31, ACC: 101, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 22, PC: 6, ADDR: 31, ACC: 101, DR: 5}
{TICK: 23, PC: 1, ADDR: 31, ACC: 101, DR: 5}
{starting executing: ['in', None]}
{TICK: 24, PC: 1, ADDR: 31, ACC: 108, DR: 5}
{TICK: 25, PC: 2, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['out', None]}
{new output symbol 'l' will be added to 'he'}
{TICK: 26, PC: 2, ADDR: 31, ACC: 108, DR: 5}
{TICK: 27, PC: 3, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['iret', None]}
{TICK: 28, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 29, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 30, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 31, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 32, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 33, PC: 6, ADDR: 31, ACC: 108, DR: 5}
{TICK: 34, PC: 1, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['in', None]}
{TICK: 35, PC: 1, ADDR: 31, ACC: 108, DR: 5}
{TICK: 36, PC: 2, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['out', None]}
{new output symbol 'l' will be added to 'hel'}
{TICK: 37, PC: 2, ADDR: 31, ACC: 108, DR: 5}
{TICK: 38, PC: 3, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['iret', None]}
{TICK: 39, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 40, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 41, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 42, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 43, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 44, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 45, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 46, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 47, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 48, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 49, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 50, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 51, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 52, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 53, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 54, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 55, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 56, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 57, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 58, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 59, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 60, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 61, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 62, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 63, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 64, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 65, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 66, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 67, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 68, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 69, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 70, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 71, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 72, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 73, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 74, PC: 5, ADDR: 31, ACC: 108, DR: 5}
...
...
{starting executing: ['jmp', ['constant', 5]]}
{TICK: 10011, PC: 5, ADDR: 31, ACC: 108, DR: 5}
{starting executing: ['jmp', ['constant', 5]]}
too long execution, increase simulation_limit, 
output: ['h', 'e', 'l', 'l'], instructions: '10000', ticks: '10012'
```


## Апробация

Вариант: `asm | acc | neum | hw | instr | struct | trap | port | prob2`

| ФИО            | Алгоритм | LoC | code инстр | инстр. | такт |
|----------------|----------|-----|------------|--------|------|
| Рогачёва Дарья | hello    | 21  | 11         |91      |148   |
| Рогачёва Дарья | cat      | 13  |13          |limit   |10012 |
| Рогачёва Дарья | prob2    | 28  |17          | 415    | 663  |
