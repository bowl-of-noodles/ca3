import pytest

from machine.isa import Opcode, OperandType
from machine.simulation import DataPath, ControlUnit
from machine.translator import RealInstruction, RealOperand


def simulate_until_finish(code, important_labels, memsize, inp):
    dp = DataPath(memsize, code, inp)
    cu = ControlUnit(dp, entrypoint_address=important_labels['start'], int_handler_address=0)

    try:
        while True:
            cu.decode_and_execute_instr()
    except StopIteration:
        return cu


@pytest.mark.parametrize("code, important_labels, expected_acc", [
    (
            [
                6,
                5,
                RealInstruction(Opcode.LD, RealOperand(OperandType.CONSTANT, 3)),
                RealInstruction(Opcode.ADD, RealOperand(OperandType.CONSTANT, 5)),
                RealInstruction(Opcode.HLT, None),
            ],
            {'start': 2},
            8,
    ),
    (
            [
                999,
                777,
                4,
                888,
                222,
                RealInstruction(Opcode.LD, RealOperand(OperandType.DIRECT_ADDRESS, 4)),
                RealInstruction(Opcode.HLT, None)
            ],
            {'start': 5},
            222,
    ),
    (
            [
                999,
                777,
                4,
                888,
                333,
                RealInstruction(Opcode.LD, RealOperand(OperandType.INDIRECT_ADDRESS, 2)),
                RealInstruction(Opcode.HLT, None)
            ],
            {'start': 5},
            333,
    ),
])
def test_addr(code, important_labels, expected_acc):
    cu = simulate_until_finish(code, important_labels, 10, [])
    assert cu.data_path.acc == expected_acc


@pytest.mark.parametrize("code, important_labels, expected_acc", [
    (
            [
                0,
                RealInstruction(Opcode.JMP, RealOperand(OperandType.CONSTANT, 3)),
                RealInstruction(Opcode.JMP, RealOperand(OperandType.CONSTANT, 999)),
                RealInstruction(Opcode.ADD, RealOperand(OperandType.CONSTANT, 5)),
                RealInstruction(Opcode.HLT, None),
            ],
            {'start': 1},
            5,
    ),
    (
            [
                0,
                RealInstruction(Opcode.LD, RealOperand(OperandType.CONSTANT, 2)),
                RealInstruction(Opcode.CMP, RealOperand(OperandType.CONSTANT, 2)),
                RealInstruction(Opcode.JE, RealOperand(OperandType.CONSTANT, 4)),
                RealInstruction(Opcode.HLT, None),
            ],
            {'start': 1},
            2,
    ),
])
def test_jmp(code, important_labels, expected_acc):
    cu = simulate_until_finish(code, important_labels, 10, [])
    assert cu.data_path.acc == expected_acc
