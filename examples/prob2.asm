section .data
    first: word 1
    second: word 2
    current: word 3
    sum: word 2

section .text
    int_handler:
    start:
        ld current
        cmp #4000000
        jb finish
        ld second
        add first
        st current
        ld second
        st first
        ld current
        st second
        mod #2
        jnz start
        ld second
        add sum
        st sum
        jmp start

    finish:
        hlt